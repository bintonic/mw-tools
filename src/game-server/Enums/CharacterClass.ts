/**
 * Class = Race * 2 + Gender
 */
export const enum CharacterClass {
	MaleHuman = 0,
	FemaleHuman = 1,
	MaleCentaur = 2,
	FemaleCentaur = 3,
	MaleMage = 4,
	FemaleMage = 5,
	MaleBorg = 6,
	FemaleBorg = 7,
}

export const enum CharacterRace {
	Human = 0,
	Centaur = 1,
	Mage = 2,
	Borg = 3,
}

export const enum CharacterGender {
	Male = 0,
	Female = 1,
}
