export const enum FightActionCommand {
	Melee = 0,
	Skill = 1,
	Item = 2,
	Defend = 3,
	Dodge = 4,
	Escape = 5,
	ChangePet = 6,
	HP = 7,
	MP = 8,
	Auto = 9,
}
