import { Species } from '../../Enums/Species';
import { FightStats } from '../Fight/FightStats';
import type { IndividualJson } from '../Individual/Individual';
import { Individual } from '../Individual/Individual';
import type { IndividualFightDataJson } from '../Individual/IndividualFightData';
import { Level } from '../Level';
import { PetFightData } from './PetFightData';

export type PetJson = IndividualJson & {
	fightData: IndividualFightDataJson;
};

// Pet IDs must be in the 0xC0000000 range.
export class Pet extends Individual {
	public fightData: PetFightData;

	public fightStats: FightStats;

	public mapData: null = null;

	public level: Level = Level.fromLevel(1);

	public icon: number = 0;

	public effect: number = 0;

	public loyalty: number = 0;

	public intimacy: number = 0;

	public species: Species = Species.Empty;

	public constructor(json: PetJson) {
		super(json);
		this.fightData = new PetFightData(json.fightData);
		this.fightStats = new FightStats(this.fightData);
	}
}
