import type { BaseItem } from '../../Database/Collections/BaseItem/BaseItemTypes';
import type { EquipmentSlot } from '../../Enums/EquipmentSlot';
import type { ClientActionContext } from '../../GameActions/GameActionContext';
import type { GameActionExecutable } from '../../GameActions/GameActionExecutable';
import type { FightStatJson } from '../Fight/FightStats';
import type { ItemType } from './ItemType';

export class Item {
	public count: number = 1;

	public locked: boolean = false;

	public price: number = 0;

	public get id(): number {
		return this.base.id;
	}

	public get file(): number {
		return this.base.file;
	}

	public get stackLimit(): number {
		return this.base.stackLimit;
	}

	public get name(): string {
		return this.base.name;
	}

	public get description(): string {
		return this.base.description;
	}

	public get type(): ItemType {
		return this.base.type;
	}

	public get equipmentSlot(): EquipmentSlot | null {
		return this.base.equipmentSlot;
	}

	public get action(): GameActionExecutable<ClientActionContext> | null {
		return this.base.action;
	}

	public get questItem(): boolean {
		return this.base.questItem;
	}

	public get stats(): FightStatJson {
		return this.base.stats ?? {};
	}

	public constructor(public base: BaseItem) {}

	public getText(): string {
		return `#Y${this.name}#N#E${this.description}`;
	}
}
