import { Resist } from '../Resist';
import type { SkillData } from '../SkillData';
import type { StatsJson } from '../Stats/Stats';
import { Stats } from '../Stats/Stats';

export type IndividualFightDataJson = {
	stats: StatsJson;
	skills: SkillData[];
};

export abstract class IndividualFightData {
	public stats: Stats;

	public resist: Resist = new Resist();

	public skills: SkillData[] = [];

	public constructor(json: IndividualFightDataJson) {
		this.stats = new Stats(json.stats);
		this.skills = json.skills;
	}
}
