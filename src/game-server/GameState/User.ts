import { CharacterGender, CharacterRace } from '../Enums/CharacterClass';
import { EquipmentSlot } from '../Enums/EquipmentSlot';
import { Skill } from '../Enums/Skill';
import type { GameConnection } from '../Server/Game/GameConnection';
import { Item } from './Item/Item';
import { Level } from './Level';
import { Pet } from './Pet/Pet';
import { Player } from './Player/Player';
import { PlayerCreator } from './Player/PlayerCreator';

export class User {
	public characters: Player[] = [];

	public constructor(public username: string, client: GameConnection) {
		let testProps = PlayerCreator.create(CharacterRace.Human, CharacterGender.Male, '');
		testProps.name = 'Player ' + testProps.id;
		let test = new Player(testProps, client.game);

		test.level = Level.fromLevel(10);
		let { stats, skills, resist } = test.fightData;
		stats.hp.pointsBase = 10;
		stats.mp.pointsBase = 10;
		stats.attack.pointsBase = 10;
		stats.speed.pointsBase = 10;
		stats.unused = 40;
		stats.healHp();
		stats.healMp();

		// Generic stats used for testing purposes
		resist.criticalRate = 10;
		resist.criticalDamage = 0;
		resist.dodgeRate = 25;

		test.items.gold = 5000;
		test.items.bankGold = 10000;

		let item = new Item(client.game.baseItems.get(1)!);
		test.items.equipment.set(EquipmentSlot.Weapon, item);

		let item2 = new Item(client.game.baseItems.get(2)!);
		item2.count = 10;
		test.items.inventory.set(0, item2);

		let pet = new Pet({
			id: 0xc0000001,
			name: 'Chicken',
			file: 234,
			fightData: {
				stats: {
					growthRate: 1.1,
					hp: { rate: 100, pointsBase: 20 },
					mp: { rate: 100, pointsBase: 20 },
					attack: { rate: 100, pointsBase: 20 },
					speed: { rate: 100, pointsBase: 20 },
					currentHp: 0,
					currentMp: 0,
					unused: 0,
				},
				skills: [],
			},
		});

		pet.fightData.stats.healHp();
		pet.fightData.stats.healMp();
		pet.fightData.stats.unused = 40;
		pet.loyalty = 100;
		test.pets.push(pet);
		test.activePet = pet;

		this.characters.push(test);
	}
}
