import type { CharacterClass, CharacterGender, CharacterRace } from '../../Enums/CharacterClass';
import { PlayerEffect } from '../../Enums/PlayerEffect';
import type { PlayerConnection } from '../../Server/Game/GameConnection';
import { Bitfield } from '../../Utils/Bitfield';
import { FightStats } from '../Fight/FightStats';
import type { Game } from '../Game';
import type { Guild } from '../Guild';
import type { IndividualJson } from '../Individual/Individual';
import { Individual } from '../Individual/Individual';
import type { IndividualFightDataJson } from '../Individual/IndividualFightData';
import type { IndividualMapDataJson } from '../Individual/IndividualMapData';
import { Level } from '../Level';
import type { Party } from '../Party';
import type { Pet } from '../Pet/Pet';
import { PlayerQuests } from '../Quest/PlayerQuests';
import { PlayerFightData } from './PlayerFightData';
import { PlayerItems } from './PlayerItems';
import { PlayerMapData } from './PlayerMapData';
import { PlayerMemory } from './PlayerMemory';
import { PlayerMisc } from './PlayerMisc';
import { PlayerTitles } from './PlayerTitles';

export type PlayerJson = IndividualJson & {
	fightData: IndividualFightDataJson;
	mapData: IndividualMapDataJson;
	gender: CharacterGender;
	race: CharacterRace;
};

export class Player extends Individual {
	public client: PlayerConnection | null = null;

	public fightData: PlayerFightData;

	public mapData: PlayerMapData;

	public titles: PlayerTitles = new PlayerTitles();

	public quests: PlayerQuests;

	public gender: CharacterGender;

	public race: CharacterRace;

	public effect: Bitfield = new Bitfield(PlayerEffect.None);

	public level: Level = Level.fromLevel(1);

	public party: Party | null = null;

	public guild: Guild | null = null;

	public misc: PlayerMisc = new PlayerMisc();

	public get class(): CharacterClass {
		return this.race * 2 + this.gender;
	}

	public items: PlayerItems = new PlayerItems(this);

	public pets: Pet[] = [];

	public activePet: Pet | null = null;

	public memory: PlayerMemory = new PlayerMemory();

	public fightStats: FightStats;

	public constructor(json: PlayerJson, public game: Game) {
		super(json);
		this.race = json.race;
		this.gender = json.gender;
		this.fightData = new PlayerFightData(json.fightData);
		this.mapData = new PlayerMapData(json.mapData, game.maps);
		this.quests = new PlayerQuests(this);
		this.fightStats = new FightStats(this.fightData, this.items.equipment);
	}

	/**
	 * Sets the file based on race, gender and reborn.
	 */
	public useDefaultFile(): void {
		this.file = this.class + 1;

		if (this.level.reborn > 0) this.file += 10;
	}
}
