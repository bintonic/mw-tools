/**
 * Manages the player's titles.
 */
export class PlayerTitles {
	/**
	 * Currently selected title.
	 */
	public title: string | null = null;

	/**
	 * Titles the player can use.
	 */
	public titles: string[] = [];
}
