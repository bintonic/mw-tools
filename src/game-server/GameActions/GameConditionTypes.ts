export type GameConditionBase<TType extends string> = {
	type: TType;
	not?: true;
};

export type GameConditionAnd = GameConditionBase<'and'> & {
	conditions: GameCondition[];
};

export type GameConditionOr = GameConditionBase<'or'> & {
	conditions: GameCondition[];
};

export type GameConditionId = GameConditionBase<'id'> & {
	id: string;
};

export type GameConditionTemplate = GameConditionBase<'template'> & {
	template: string;
	params?: Record<string, unknown>;
};

export type GameConditionRandom = GameConditionBase<'random'> & {
	/** Chance from 0 to 100 */
	chance: number;
};

export type GameConditionQuest = GameConditionBase<'quest'> & {
	quest: number;
	stage?: number;
};

export type GameConditionItem = GameConditionBase<'item'> & {
	baseItemId: number;
	count?: number;
	includeBank?: boolean;
};

export type GameConditionItemSpace = GameConditionBase<'itemSpace'> & {
	baseItemId?: number;
	count?: number;
};

export type GameConditionGold = GameConditionBase<'gold'> & {
	amount: number;
};

export type GameConditionLevel = GameConditionBase<'level'> & {
	min?: number,
	max?: number,
	inclusive?: boolean,
};

export type GameConditionSingle =
	| null
	| GameConditionAnd
	| GameConditionOr
	| GameConditionId
	| GameConditionTemplate
	| GameConditionRandom
	| GameConditionQuest
	| GameConditionItem
	| GameConditionItemSpace
	| GameConditionGold
	| GameConditionLevel;

export type GameCondition = string | string[] | GameConditionSingle | GameConditionSingle[];
