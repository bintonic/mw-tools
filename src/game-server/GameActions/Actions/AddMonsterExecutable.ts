import { getConfig } from '../../Config/Config';
import type { MonsterTemplate } from '../../Data/MonsterTemplates';
import { monsterTemplates } from '../../Data/MonsterTemplates';
import type { Player } from '../../GameState/Player/Player';
import { PetPackets } from '../../Responses/PetPackets';
import { PlayerPackets } from '../../Responses/PlayerPackets';
import type { ClientActionContext } from '../GameActionContext';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionMonster } from '../GameActionTypes';

/**
 * Calculate how much exp to give based on level of monster, expBase and player level
 * If level difference between monster and player is to great then it will only reward 144 exp
 * @param player
 * @param expBase
 * @param monster
 * @returns
 */
export function calculateMonsterExp(player: Player, expBase: number, monsterLevel: number): number {
	let exp = expBase * monsterLevel;

	if (Math.abs(monsterLevel - player.level.level) > 10 && exp > 144) {
		exp = 144;
	}

	return exp * getConfig().modifiers.exp;
}

/**
 * Calculate how much gold to give based on level of monster, goldBase and player level
 * If level difference between monster and player is to great then it will only reward 144 gold
 * @param player
 * @param monster
 * @returns
 */
export function calculateMonsterGold(
	player: Player,
	goldBase: number,
	monsterLevel: number,
): number {
	let gold = goldBase * monsterLevel;

	if (Math.abs(monsterLevel - player.level.level) > 10 && gold > 144) {
		gold = 144;
	}

	return gold * getConfig().modifiers.gold;
}

/**
 * Give exp and gold to the player and pet from monster kills.
 */
export class AddMonsterExecutable extends GameActionExecutable<ClientActionContext> {
	protected constructor(protected override readonly action: GameActionMonster) {
		super(action);
	}

	public static parse(action: GameActionMonster): AddMonsterExecutable {
		return new this(action);
	}

	protected run(context: ClientActionContext): void {
		if (monsterTemplates[this.action.monster]) {
			let monster = monsterTemplates[this.action.monster];
			this.addGold(context.player, monster);
			this.addExp(context, monster);
		}
	}

	/**
	 * Distribute gold to the player
	 * @param goldBase
	 * @param player
	 * @param monster
	 */
	private addGold(player: Player, monster: MonsterTemplate): void {
		if (monster.rewards) {
			let gold = calculateMonsterGold(player, monster.rewards?.goldBase, monster.level);
			if (gold > 0) player.items.addGoldAndSend(gold);
		}
	}

	/**
	 * Distribute exp to the player and pet
	 * @param expBase
	 * @param param1
	 * @param monster
	 */
	private addExp({ client, player }: ClientActionContext, monster: MonsterTemplate): void {
		if (monster.rewards) {
			let playerExp = calculateMonsterExp(player, monster.rewards.expBase, monster.level);

			let pet = player.activePet;
			let petExp = 0;

			// If using a pet then add exp to it
			if (pet) {
				petExp = playerExp * 0.25;
				playerExp = playerExp - petExp;

				let petLevels = pet.level.addExp(petExp);

				if (petLevels === 0) {
					client.write(PetPackets.experience(pet));
				} else {
					pet.fightData.stats.updateStatPointsForLevel(pet.level.level);
					client.write(PetPackets.level(pet));
				}
			}

			// Add exp to the player
			let playerLevels = player.level.addExp(playerExp);
			if (playerLevels === 0) {
				client.write(PlayerPackets.experience(player));
			} else {
				player.fightData.stats.updateStatPointsForLevel(player.level.level);

				client.write(PlayerPackets.level(player));
			}
		}
	}
}
