import { Species } from '../Enums/Species';
import type { GameAction } from '../GameActions/GameActionTypes';
import type { MonsterRewardsJson } from '../GameState/Monster/MonsterRewards';
import type { BaseStats, RandomStatRates } from '../GameState/Stats/StatRates';

/**
 * Template used to create monsters and pets.
 */
export type BaseMonsterTemplate = {
	name: string;
	file: number;
	statRates: RandomStatRates;
	species: Species;
};

/**
 * A monster the player can fight against.
 */
export type MonsterTemplate = BaseMonsterTemplate & {
	build: BaseStats; // Not the final stats, will be multiplied to match level
	level: number;
	rewards?: MonsterRewardsJson;
	onMonsterPlayerFightWin?: GameAction | null;
};

// Temporary, will be moved to db
export const monsterTemplates: Record<string, MonsterTemplate> = {
	greenCapilla: {
		name: 'Green Capilla',
		file: 254,
		species: Species.Undead,
		statRates: {
			growthRate: { min: 1.184, max: 1.213 },
			sta: { min: 86, max: 86 },
			int: { min: 238, max: 238 },
			str: { min: 18, max: 18 },
			agi: { min: 34, max: 34 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 },
		level: 2,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'greenCapilla',
			},
		],
	},
	greenCapillaBoss: {
		name: 'Green Capilla Boss',
		file: 254,
		species: Species.Undead,
		statRates: {
			growthRate: { min: 1.184, max: 1.213 },
			sta: { min: 86, max: 86 },
			int: { min: 238, max: 238 },
			str: { min: 18, max: 18 },
			agi: { min: 34, max: 34 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 },
		level: 4,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'greenCapillaBoss',
			},
		],
	},
	teethor: {
		name: 'Teethor',
		file: 222,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.158, max: 1.182 },
			sta: { min: 85, max: 85 },
			int: { min: 224, max: 224 },
			str: { min: 16, max: 16 },
			agi: { min: 115, max: 115 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 },
		level: 5,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'teethor',
			},
		],
	},
	antEater: {
		name: 'Ant Eater',
		file: 205,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.149, max: 1.179 },
			sta: { min: 145, max: 145 },
			int: { min: 52, max: 52 },
			str: { min: 18, max: 18 },
			agi: { min: 69, max: 69 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 },
		level: 6,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'antEater',
			},
		],
	},
	battleInstructor: {
		name: 'Battle Instructor',
		file: 125,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.149, max: 1.179 },
			sta: { min: 150, max: 160 },
			int: { min: 52, max: 52 },
			str: { min: 20, max: 30 },
			agi: { min: 69, max: 69 },
		},
		build: { sta: 3, int: 1, str: 2, agi: 1 },
		level: 5,
	},
	nepenthes: {
		name: 'Nepenthes',
		file: 223,
		species: Species.Demon,
		statRates: {
			growthRate: { min: 1.144, max: 1.174 },
			sta: { min: 95, max: 95 },
			int: { min: 147, max: 147 },
			str: { min: 19, max: 19 },
			agi: { min: 100, max: 100 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 8,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'nepenthes',
			},
		],
	},
	evilAntEater: {
		name: 'Evil Ant Eater',
		file: 205,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.149, max: 1.179 },
			sta: { min: 145, max: 145 },
			int: { min: 52, max: 52 },
			str: { min: 18, max: 18 },
			agi: { min: 69, max: 69 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 8,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'evilAntEater',
			},
		],
	},
	poisonTeethor: {
		name: 'Poison Teethor',
		file: 222,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.158, max: 1.182 },
			sta: { min: 85, max: 85 },
			int: { min: 224, max: 224 },
			str: { min: 16, max: 16 },
			agi: { min: 115, max: 115 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 12,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'poisonTeethor',
			},
		],
	},
	earthWolf: {
		name: 'Earth Wolf',
		file: 207,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.166, max: 1.196 },
			sta: { min: 96, max: 96 },
			int: { min: 69, max: 69 },
			str: { min: 26, max: 26 },
			agi: { min: 144, max: 144 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 13,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'earthWolf',
			},
		],
	},
	evilNepenthes: {
		name: 'Evil Nepenthes',
		file: 223,
		species: Species.Demon,
		statRates: {
			growthRate: { min: 1.144, max: 1.174 },
			sta: { min: 95, max: 95 },
			int: { min: 147, max: 147 },
			str: { min: 19, max: 19 },
			agi: { min: 100, max: 100 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 14,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'evilNepenthes',
			},
		],
	},
	flowerSpirit: {
		name: 'Flower Spirit',
		file: 223, //TODO
		species: Species.Special, //TODO
		statRates: {
			//TODO
			growthRate: { min: 1.158, max: 1.182 },
			sta: { min: 85, max: 85 },
			int: { min: 224, max: 224 },
			str: { min: 16, max: 16 },
			agi: { min: 115, max: 115 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 16, //TODO
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'flowerSpirit',
			},
		],
	},
	lizardBandit: {
		name: 'Lizard Bandit',
		file: 224,
		species: Species.Human,
		statRates: {
			growthRate: { min: 1.134, max: 1.164 },
			sta: { min: 88, max: 88 },
			int: { min: 112, max: 112 },
			str: { min: 18, max: 18 },
			agi: { min: 62, max: 62 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 46,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'lizardBandit',
			},
		],
	},
	greaterWolf: {
		name: 'Greater Wolf',
		file: 207,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.3, max: 1.3 },
			sta: { min: 130, max: 130 },
			int: { min: 286, max: 286 },
			str: { min: 49, max: 49 },
			agi: { min: 141, max: 141 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 47,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'greaterWolf',
			},
		],
	},
	madOrgewalker: {
		name: 'Mad Ogrewalker',
		file: 252,
		species: Species.Human,
		statRates: {
			growthRate: { min: 1.164, max: 1.194 },
			sta: { min: 123, max: 123 },
			int: { min: 48, max: 48 },
			str: { min: 42, max: 42 },
			agi: { min: 60, max: 60 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 48,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'madOrgewalker',
			},
		],
	},
	wanderer: {
		name: 'Wanderer',
		file: 249, //TODO
		species: Species.Human, //TODO
		statRates: {
			//TODO
			growthRate: { min: 1.158, max: 1.182 },
			sta: { min: 85, max: 85 },
			int: { min: 224, max: 224 },
			str: { min: 16, max: 16 },
			agi: { min: 115, max: 115 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 49,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'wanderer',
			},
		],
	},
	snakeDemon: {
		name: 'Snake Demon',
		file: 251,
		species: Species.Demon,
		statRates: {
			growthRate: { min: 1.139, max: 1.169 },
			sta: { min: 89, max: 89 },
			int: { min: 483, max: 483 },
			str: { min: 16, max: 16 },
			agi: { min: 129, max: 129 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 50,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'snakeDemon',
			},
		],
	},
	thornSpider: {
		name: 'Thorn Spider',
		file: 227,
		species: Species.Special,
		statRates: {
			growthRate: { min: 1.162, max: 1.19 },
			sta: { min: 88, max: 88 },
			int: { min: 192, max: 192 },
			str: { min: 22, max: 22 },
			agi: { min: 94, max: 94 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 51,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'thornSpider',
			},
		],
	},
	swordSpider: {
		name: 'Sword Spider',
		file: 225,
		species: Species.Special,
		statRates: {
			//TODO
			growthRate: { min: 1.3, max: 1.3 },
			sta: { min: 85, max: 85 },
			int: { min: 224, max: 224 },
			str: { min: 16, max: 16 },
			agi: { min: 115, max: 115 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 52,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'swordSpider',
			},
		],
	},
	iceDemon: {
		name: 'Ice Demon',
		file: 231,
		species: Species.Dragon,
		statRates: {
			growthRate: { min: 1.169, max: 1.194 },
			sta: { min: 110, max: 110 },
			int: { min: 339, max: 339 },
			str: { min: 18, max: 18 },
			agi: { min: 81, max: 81 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 53,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'iceDemon',
			},
		],
	},
	magicalDemon: {
		name: 'Magical Demon',
		file: 253,
		species: Species.Dragon,
		statRates: {
			growthRate: { min: 1.171, max: 1.193 },
			sta: { min: 132, max: 132 },
			int: { min: 288, max: 288 },
			str: { min: 23, max: 23 },
			agi: { min: 82, max: 82 },
		},
		build: { sta: 1, int: 1, str: 1, agi: 1 }, //TODO
		level: 54,
		rewards: {
			expBase: 3,
			goldBase: 2,
		},
		onMonsterPlayerFightWin: [
			{
				type: 'monster',
				monster: 'magicalDemon',
			},
		],
	},
};
