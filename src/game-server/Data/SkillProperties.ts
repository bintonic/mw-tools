import type { Skill } from '../Enums/Skill';
import { SkillGroup } from '../Enums/SkillGroup';

export type SkillProperties = {
	id: Skill;
	group: SkillGroup;
	rank: 1 | 2 | 3 | 4; // I, II, III or IV skill
	mp: number; // Initial mana cost
	mpAdd: number; // Mana increased for each use
	rounds: number; // How many rounds this lasts (at lvl 1)
	targets: number; // How many targets it hits (at lvl 1)
	single: boolean; // True for I and III skills
	enemy: boolean; // Targets the enemy
};

//Group, rank, mp, mpAdd, rounds, targets, enemy
type SkillPropsCompact = [SkillGroup, 1 | 2 | 3 | 4, number, number, number, number, boolean];

// prettier-ignore
let propsCompact: SkillPropsCompact[] = [
	[SkillGroup.Frailty,	1,	35,		0.09,	3,	1,	true],
	[SkillGroup.Frailty,	2,	80,		0.22,	2,	2,	true],
	[SkillGroup.Frailty,	3,	450,	0.48,	3,	1,	true],
	[SkillGroup.Frailty,	4,	1050,	0.91,	2,	3,	true],
	[SkillGroup.Poison,		1,	35,		0.09,	3,	1,	true],
	[SkillGroup.Poison,		2,	80,		0.22,	2,	2,	true],
	[SkillGroup.Poison,		3,	450,	0.48,	3,	1,	true],
	[SkillGroup.Poison,		4,	1050,	0.91,	2,	3,	true],
	[SkillGroup.Chaos,		1,	35,		0.09,	3,	1,	true],
	[SkillGroup.Chaos,		2,	80,		0.22,	2,	2,	true],
	[SkillGroup.Chaos,		3,	450,	0.48,	3,	1,	true],
	[SkillGroup.Chaos,		4,	1050,	0.91,	2,	3,	true],
	[SkillGroup.Hypnotize,	1,	35,		0.09,	3,	1,	true],
	[SkillGroup.Hypnotize,	2,	80,		0.22,	2,	2,	true],
	[SkillGroup.Hypnotize,	3,	450,	0.48,	3,	1,	true],
	[SkillGroup.Hypnotize,	4,	1050,	0.91,	2,	3,	true],
	[SkillGroup.Stun,		1,	35,		0.09,	3,	1,	true],
	[SkillGroup.Stun,		2,	80,		0.22,	2,	2,	true],
	[SkillGroup.Stun,		3,	450,	0.48,	3,	1,	true],
	[SkillGroup.Stun,		4,	1050,	0.91,	2,	3,	true],
	[SkillGroup.PurgeChaos,	1,	60,		0.12,	0,	1,	false],
	[SkillGroup.PurgeChaos,	2,	140,	0.26,	0,	2,	false],
	[SkillGroup.PurgeChaos,	3,	600,	0.51,	0,	1,	false],
	[SkillGroup.PurgeChaos,	4,	1400,	0.99,	0,	3,	false],
	[SkillGroup.UnStun,		1,	60,		0.12,	0,	1,	false],
	[SkillGroup.UnStun,		2,	140,	0.26,	0,	2,	false],
	[SkillGroup.UnStun,		3,	600,	0.51,	0,	1,	false],
	[SkillGroup.UnStun,		4,	1400,	0.99,	0,	3,	false],
	[SkillGroup.MultiShot,	1,	30,		0.1,	0,	1,	true],
	[SkillGroup.MultiShot,	2,	70,		0.28,	0,	2,	true],
	[SkillGroup.MultiShot,	3,	500,	0.58,	0,	1,	true],
	[SkillGroup.MultiShot,	4,	1200,	1.12,	0,	3,	true],
	[SkillGroup.Blizzard,	1,	30,		0.1,	0,	1,	true],
	[SkillGroup.Blizzard,	2,	70,		0.28,	0,	2,	true],
	[SkillGroup.Blizzard,	3,	500,	0.58,	0,	1,	true],
	[SkillGroup.Blizzard,	4,	1200,	1.12,	0,	3,	true],
	[SkillGroup.Speed,		1,	30,		0.09,	3,	1,	false],
	[SkillGroup.Speed,		2,	70,		0.22,	2,	2,	false],
	[SkillGroup.Speed,		3,	500,	0.48,	3,	1,	false],
	[SkillGroup.Speed,		4,	1200,	0.91,	2,	3,	false],
	[SkillGroup.HealOther,	1,	80,		0.22,	0,	1,	false],
	[SkillGroup.HealOther,	2,	180,	0.51,	0,	2,	false],
	[SkillGroup.HealOther,	3,	850,	0.66,	0,	1,	false],
	[SkillGroup.HealOther,	4,	2000,	1.12,	0,	3,	false],
	[SkillGroup.Fire,		1,	40,		0.1,	0,	1,	true],
	[SkillGroup.Fire,		2,	105,	0.28,	0,	2,	true],
	[SkillGroup.Fire,		3,	600,	0.58,	0,	1,	true],
	[SkillGroup.Fire,		4,	1400,	1.12,	0,	3,	true],
	[SkillGroup.Ice,		1,	40,		0.1,	0,	1,	true],
	[SkillGroup.Ice,		2,	105,	0.28,	0,	2,	true],
	[SkillGroup.Ice,		3,	600,	0.58,	0,	1,	true],
	[SkillGroup.Ice,		4,	1400,	1.12,	0,	3,	true],
	[SkillGroup.Evil,		1,	40,		0.1,	0,	1,	true],
	[SkillGroup.Evil,		2,	105,	0.28,	0,	2,	true],
	[SkillGroup.Evil,		3,	600,	0.58,	0,	1,	true],
	[SkillGroup.Evil,		4,	1400,	1.12,	0,	3,	true],
	[SkillGroup.Flash,		1,	40,		0.1,	0,	1,	true],
	[SkillGroup.Flash,		2,	105,	0.28,	0,	2,	true],
	[SkillGroup.Flash,		3,	600,	0.58,	0,	1,	true],
	[SkillGroup.Flash,		4,	1400,	1.12,	0,	3,	true],
	[SkillGroup.Death,		1,	40,		0.1,	9,	1,	true],
	[SkillGroup.Death,		2,	105,	0.28,	10,	2,	true],
	[SkillGroup.Death,		3,	600,	0.58,	6,	1,	true],
	[SkillGroup.Death,		4,	1400,	1.12,	8,	3,	true],
	[SkillGroup.Enhance,	1,	20,		0.09,	3,	1,	false],
	[SkillGroup.Enhance,	2,	47,		0.22,	2,	2,	false],
	[SkillGroup.Enhance,	3,	280,	0.48,	3,	1,	false],
	[SkillGroup.Enhance,	4,	650,	0.91,	2,	3,	false],
	[SkillGroup.Protect,	1,	20,		0.09,	3,	1,	false],
	[SkillGroup.Protect,	2,	47,		0.22,	2,	2,	false],
	[SkillGroup.Protect,	3,	280,	0.48,	3,	1,	false],
	[SkillGroup.Protect,	4,	650,	0.91,	2,	3,	false],
	[SkillGroup.Drain,		1,	20,		0.1,	0,	1,	true],
	[SkillGroup.Drain,		2,	47,		0.28,	0,	2,	true],
	[SkillGroup.Drain,		3,	280,	0.58,	0,	1,	true],
	[SkillGroup.Drain,		4,	650,	1.12,	0,	3,	true],
	[SkillGroup.Reflect,	1,	20,		0.09,	3,	1,	false],
	[SkillGroup.Reflect,	2,	47,		0.22,	2,	2,	false],
	[SkillGroup.Reflect,	3,	280,	0.48,	3,	1,	false],
	[SkillGroup.Reflect,	4,	650,	0.91,	2,	3,	false],
	[SkillGroup.Repel,		1,	20,		0.09,	3,	1,	false],
	[SkillGroup.Repel,		2,	47,		0.22,	2,	2,	false],
	[SkillGroup.Repel,		3,	280,	0.48,	3,	1,	false],
	[SkillGroup.Repel,		4,	650,	0.91,	2,	3,	false],
];

export const skillProperties: Record<number, Readonly<SkillProperties>> = {};

propsCompact.forEach(skill => {
	let skillId = skill[0] * 4 + skill[1] - 1;

	skillProperties[skillId] = {
		id: skillId,
		group: skill[0],
		rank: skill[1],
		mp: skill[2],
		mpAdd: skill[3],
		rounds: skill[4],
		targets: skill[5],
		single: skill[1] === 1 || skill[1] === 3,
		enemy: skill[6],
	};
});
