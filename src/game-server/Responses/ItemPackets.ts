import type { EquipmentSlot } from '../Enums/EquipmentSlot';
import type { Item } from '../GameState/Item/Item';
import type { ItemContainer } from '../GameState/Item/ItemContainer';
import type { PlayerItems } from '../GameState/Player/PlayerItems';
import { Packet } from '../PacketBuilder';
import { PacketType } from '../PacketType';
import { ItemStruct } from './Structs/ItemStruct';

export const enum ItemEquipStatus {
	Ok = 1,
	LowInt = -1,
	LowStr = -2,
	LowAgi = -3,
	LowSta = -4,
	LowLvl = -5,
	WrongRace = -6,
}

export abstract class ItemPackets {
	/**
	 * Send the player's inventory to the client.
	 * @param inventory
	 */
	public static inventory(inventory: ItemContainer): Packet {
		let packet = new Packet(
			16 + inventory.usedSize * ItemStruct.size,
			PacketType.InventoryData,
		).uint8(12, inventory.usedSize);

		ItemStruct.writeList(packet, 16, inventory.entries());
		return packet;
	}

	// TODO is this used?
	// public static add(): Buffer{}

	/**
	 * Remove an item from the client inventory.
	 * @param index slot number
	 */
	public static remove(index: number): Packet {
		return new Packet(16, PacketType.ItemRemove).uint8(12, index);
	}

	/**
	 * Update specific slots in the client's inventory.
	 * Null empties the slot.
	 * TODO: slot >= 0x01000000 targets different client memory area, maybe shop or bank?
	 * @param items
	 */
	public static change(items: [slot: number, item: Item | null][]): Packet {
		let packet = new Packet(16 + items.length * ItemStruct.size, PacketType.ItemChange).uint8(
			12,
			items.length,
		);

		ItemStruct.writeList(packet, 16, items);
		return packet;
	}

	/**
	 * Update the client's gold (both inventory and bank).
	 * @param playerItems
	 */
	public static gold(playerItems: PlayerItems): Packet {
		return new Packet(24, PacketType.GoldData)
			.uint32(16, playerItems.gold)
			.uint32(20, playerItems.bankGold);
	}

	// TODO
	// public static give(): Buffer{}

	/**
	 * Update a single equipment slot.
	 * @param status
	 * @param slot
	 * @param item
	 */
	public static equip(status: ItemEquipStatus, slot: EquipmentSlot, item: Item | null): Packet {
		return new Packet(16 + ItemStruct.size, PacketType.ItemEquip)
			.int32(12, status)
			.struct(16, ItemStruct, slot, item);
	}

	/**
	 * Update all equipment slots.
	 * @param equipment
	 */
	public static equipment(equipment: ItemContainer): Packet {
		let packet = new Packet(
			16 + equipment.usedSize * ItemStruct.size,
			PacketType.EquipmentList,
		).uint8(12, equipment.usedSize);

		ItemStruct.writeList(packet, 16, equipment.entries());
		return packet;
	}

	/**
	 * Send the text of an item description.
	 * @param item
	 */
	public static info(item: Item): Packet {
		let text = item.getText();
		return new Packet(16 + text.length + 1, PacketType.ItemInfo).string(16, text);
	}
}
