import type { NpcJson } from '../../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../../Enums/Direction';

export const ShowCaseNpcsData: NpcJson[] = [
	{
		id: 0x88800001,
		name: '1',
		file: 1,
		map: 109,
		point: { x: 3120, y: 2168 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800002,
		name: '2',
		file: 2,
		map: 109,
		point: { x: 3072, y: 2132 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800003,
		name: '3',
		file: 3,
		map: 109,
		point: { x: 3024, y: 2096 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800004,
		name: '4',
		file: 4,
		map: 109,
		point: { x: 2976, y: 2060 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800005,
		name: '5',
		file: 5,
		map: 109,
		point: { x: 2928, y: 2024 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800006,
		name: '6',
		file: 6,
		map: 109,
		point: { x: 2880, y: 1988 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800007,
		name: '7',
		file: 7,
		map: 109,
		point: { x: 2832, y: 1952 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800008,
		name: '8',
		file: 8,
		map: 109,
		point: { x: 2784, y: 1916 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800009,
		name: '11',
		file: 11,
		map: 109,
		point: { x: 2736, y: 1880 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800010,
		name: '12',
		file: 12,
		map: 109,
		point: { x: 2688, y: 1844 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800011,
		name: '13',
		file: 13,
		map: 109,
		point: { x: 2640, y: 1808 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800012,
		name: '14',
		file: 14,
		map: 109,
		point: { x: 2592, y: 1772 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800013,
		name: '15',
		file: 15,
		map: 109,
		point: { x: 2544, y: 1736 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800014,
		name: '16',
		file: 16,
		map: 109,
		point: { x: 2496, y: 1700 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800015,
		name: '17',
		file: 17,
		map: 109,
		point: { x: 2448, y: 1664 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800016,
		name: '18',
		file: 18,
		map: 109,
		point: { x: 2400, y: 1628 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800017,
		name: '101',
		file: 101,
		map: 109,
		point: { x: 2352, y: 1592 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800018,
		name: '102',
		file: 102,
		map: 109,
		point: { x: 2304, y: 1556 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800019,
		name: '103',
		file: 103,
		map: 109,
		point: { x: 2256, y: 1520 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800020,
		name: '104',
		file: 104,
		map: 109,
		point: { x: 2208, y: 1484 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800021,
		name: '105',
		file: 105,
		map: 109,
		point: { x: 2160, y: 1448 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800022,
		name: '106',
		file: 106,
		map: 109,
		point: { x: 2112, y: 1412 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800023,
		name: '107',
		file: 107,
		map: 109,
		point: { x: 2064, y: 1376 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800024,
		name: '108',
		file: 108,
		map: 109,
		point: { x: 2016, y: 1340 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800025,
		name: '109',
		file: 109,
		map: 109,
		point: { x: 1968, y: 1304 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800026,
		name: '110',
		file: 110,
		map: 109,
		point: { x: 1920, y: 1268 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800027,
		name: '111',
		file: 111,
		map: 109,
		point: { x: 1872, y: 1232 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800028,
		name: '112',
		file: 112,
		map: 109,
		point: { x: 1824, y: 1196 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800029,
		name: '113',
		file: 113,
		map: 109,
		point: { x: 1776, y: 1160 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800030,
		name: '114',
		file: 114,
		map: 109,
		point: { x: 1728, y: 1124 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800031,
		name: '115',
		file: 115,
		map: 109,
		point: { x: 1680, y: 1088 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800032,
		name: '116',
		file: 116,
		map: 109,
		point: { x: 1632, y: 1052 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800033,
		name: '117',
		file: 117,
		map: 109,
		point: { x: 1584, y: 1016 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800034,
		name: '118',
		file: 118,
		map: 109,
		point: { x: 1536, y: 980 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800035,
		name: '119',
		file: 119,
		map: 109,
		point: { x: 1488, y: 944 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800036,
		name: '120',
		file: 120,
		map: 109,
		point: { x: 1440, y: 908 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800037,
		name: '121',
		file: 121,
		map: 109,
		point: { x: 1392, y: 872 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800038,
		name: '122',
		file: 122,
		map: 109,
		point: { x: 1344, y: 836 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800039,
		name: '123',
		file: 123,
		map: 109,
		point: { x: 1296, y: 800 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800040,
		name: '124',
		file: 124,
		map: 109,
		point: { x: 1248, y: 764 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800041,
		name: '125',
		file: 125,
		map: 109,
		point: { x: 1200, y: 728 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800042,
		name: '126',
		file: 126,
		map: 109,
		point: { x: 1152, y: 692 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800043,
		name: '128',
		file: 128,
		map: 109,
		point: { x: 1104, y: 656 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800044,
		name: '129',
		file: 129,
		map: 109,
		point: { x: 1056, y: 620 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800045,
		name: '130',
		file: 130,
		map: 109,
		point: { x: 1008, y: 584 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800046,
		name: '131',
		file: 131,
		map: 109,
		point: { x: 960, y: 548 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800047,
		name: '132',
		file: 132,
		map: 109,
		point: { x: 912, y: 512 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800048,
		name: '133',
		file: 133,
		map: 109,
		point: { x: 864, y: 476 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800049,
		name: '134',
		file: 134,
		map: 109,
		point: { x: 816, y: 440 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800050,
		name: '135',
		file: 135,
		map: 109,
		point: { x: 768, y: 404 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800051,
		name: '136',
		file: 136,
		map: 109,
		point: { x: 720, y: 368 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800052,
		name: '137',
		file: 137,
		map: 109,
		point: { x: 672, y: 332 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800053,
		name: '138',
		file: 138,
		map: 109,
		point: { x: 624, y: 296 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800054,
		name: '139',
		file: 139,
		map: 109,
		point: { x: 576, y: 260 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800055,
		name: '140',
		file: 140,
		map: 109,
		point: { x: 2944, y: 256 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800056,
		name: '144',
		file: 144,
		map: 109,
		point: { x: 2192, y: 1056 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800057,
		name: '145',
		file: 145,
		map: 109,
		point: { x: 2336, y: 1160 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800058,
		name: '146',
		file: 146,
		map: 109,
		point: { x: 2480, y: 1265 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800059,
		name: '147',
		file: 147,
		map: 109,
		point: { x: 2624, y: 1368 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800060,
		name: '148',
		file: 148,
		map: 109,
		point: { x: 2768, y: 1472 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800061,
		name: '149',
		file: 149,
		map: 109,
		point: { x: 2912, y: 1576 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800062,
		name: '150',
		file: 150,
		map: 109,
		point: { x: 2720, y: 368 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800063,
		name: '153',
		file: 153,
		map: 109,
		point: { x: 2688, y: 384 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800064,
		name: '157',
		file: 157,
		map: 109,
		point: { x: 2656, y: 400 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800065,
		name: '158',
		file: 158,
		map: 109,
		point: { x: 2624, y: 416 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800066,
		name: '159',
		file: 159,
		map: 109,
		point: { x: 2592, y: 432 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800067,
		name: '160',
		file: 160,
		map: 109,
		point: { x: 2560, y: 448 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800068,
		name: '161',
		file: 161,
		map: 109,
		point: { x: 2528, y: 464 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800069,
		name: '162',
		file: 162,
		map: 109,
		point: { x: 2496, y: 480 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800070,
		name: '163',
		file: 163,
		map: 109,
		point: { x: 2464, y: 496 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800071,
		name: '164',
		file: 164,
		map: 109,
		point: { x: 2432, y: 512 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800072,
		name: '201',
		file: 201,
		map: 109,
		point: { x: 2400, y: 528 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800073,
		name: '202',
		file: 202,
		map: 109,
		point: { x: 2368, y: 544 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800074,
		name: '203',
		file: 203,
		map: 109,
		point: { x: 2336, y: 560 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800075,
		name: '204',
		file: 204,
		map: 109,
		point: { x: 2304, y: 576 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800076,
		name: '205',
		file: 205,
		map: 109,
		point: { x: 2272, y: 592 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800077,
		name: '206',
		file: 206,
		map: 109,
		point: { x: 2240, y: 608 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800078,
		name: '207',
		file: 207,
		map: 109,
		point: { x: 2208, y: 624 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800079,
		name: '208',
		file: 208,
		map: 109,
		point: { x: 2176, y: 640 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800080,
		name: '209',
		file: 209,
		map: 109,
		point: { x: 2144, y: 656 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800081,
		name: '210',
		file: 210,
		map: 109,
		point: { x: 2112, y: 672 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800082,
		name: '211',
		file: 211,
		map: 109,
		point: { x: 2080, y: 688 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800083,
		name: '212',
		file: 212,
		map: 109,
		point: { x: 2048, y: 704 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800084,
		name: '213',
		file: 213,
		map: 109,
		point: { x: 2016, y: 720 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800085,
		name: '214',
		file: 214,
		map: 109,
		point: { x: 1984, y: 736 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800086,
		name: '215',
		file: 215,
		map: 109,
		point: { x: 1952, y: 752 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800087,
		name: '217',
		file: 217,
		map: 109,
		point: { x: 1920, y: 768 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800088,
		name: '218',
		file: 218,
		map: 109,
		point: { x: 1888, y: 784 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800089,
		name: '219',
		file: 219,
		map: 109,
		point: { x: 1856, y: 800 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800090,
		name: '220',
		file: 220,
		map: 109,
		point: { x: 1824, y: 816 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800091,
		name: '221',
		file: 221,
		map: 109,
		point: { x: 1792, y: 832 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800092,
		name: '222',
		file: 222,
		map: 109,
		point: { x: 1760, y: 848 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800093,
		name: '223',
		file: 223,
		map: 109,
		point: { x: 1728, y: 864 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800094,
		name: '224',
		file: 224,
		map: 109,
		point: { x: 1696, y: 880 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800095,
		name: '225',
		file: 225,
		map: 109,
		point: { x: 1664, y: 896 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800096,
		name: '226',
		file: 226,
		map: 109,
		point: { x: 1632, y: 912 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800097,
		name: '227',
		file: 227,
		map: 109,
		point: { x: 1600, y: 928 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800098,
		name: '228',
		file: 228,
		map: 109,
		point: { x: 1568, y: 944 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800099,
		name: '229',
		file: 229,
		map: 109,
		point: { x: 1536, y: 960 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800100,
		name: '230',
		file: 230,
		map: 109,
		point: { x: 1504, y: 976 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800101,
		name: '231',
		file: 231,
		map: 109,
		point: { x: 1472, y: 992 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800102,
		name: '232',
		file: 232,
		map: 109,
		point: { x: 1440, y: 1008 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800103,
		name: '233',
		file: 233,
		map: 109,
		point: { x: 1408, y: 1024 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800104,
		name: '234',
		file: 234,
		map: 109,
		point: { x: 1376, y: 1040 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800105,
		name: '235',
		file: 235,
		map: 109,
		point: { x: 1344, y: 1056 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800106,
		name: '236',
		file: 236,
		map: 109,
		point: { x: 1312, y: 1072 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800107,
		name: '237',
		file: 237,
		map: 109,
		point: { x: 1280, y: 1088 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800108,
		name: '238',
		file: 238,
		map: 109,
		point: { x: 1248, y: 1104 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800109,
		name: '239',
		file: 239,
		map: 109,
		point: { x: 1216, y: 1120 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800110,
		name: '240',
		file: 240,
		map: 109,
		point: { x: 1184, y: 1136 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800111,
		name: '241',
		file: 241,
		map: 109,
		point: { x: 1152, y: 1152 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800112,
		name: '242',
		file: 242,
		map: 109,
		point: { x: 1120, y: 1168 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800113,
		name: '243',
		file: 243,
		map: 109,
		point: { x: 1088, y: 1184 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800114,
		name: '244',
		file: 244,
		map: 109,
		point: { x: 1056, y: 1200 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800115,
		name: '245',
		file: 245,
		map: 109,
		point: { x: 1024, y: 1216 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800116,
		name: '246',
		file: 246,
		map: 109,
		point: { x: 992, y: 1232 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800117,
		name: '247',
		file: 247,
		map: 109,
		point: { x: 960, y: 1248 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800118,
		name: '248',
		file: 248,
		map: 109,
		point: { x: 928, y: 1264 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800119,
		name: '249',
		file: 249,
		map: 109,
		point: { x: 896, y: 1280 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800120,
		name: '250',
		file: 250,
		map: 109,
		point: { x: 864, y: 1296 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800121,
		name: '251',
		file: 251,
		map: 109,
		point: { x: 832, y: 1312 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800122,
		name: '252',
		file: 252,
		map: 109,
		point: { x: 800, y: 1328 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800123,
		name: '253',
		file: 253,
		map: 109,
		point: { x: 768, y: 1344 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800124,
		name: '254',
		file: 254,
		map: 109,
		point: { x: 736, y: 1360 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800125,
		name: '255',
		file: 255,
		map: 109,
		point: { x: 704, y: 1376 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800126,
		name: '256',
		file: 256,
		map: 109,
		point: { x: 672, y: 1392 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800127,
		name: '258',
		file: 258,
		map: 109,
		point: { x: 640, y: 1408 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800128,
		name: '259',
		file: 259,
		map: 109,
		point: { x: 608, y: 1424 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800129,
		name: '260',
		file: 260,
		map: 109,
		point: { x: 576, y: 1440 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800130,
		name: '262',
		file: 262,
		map: 109,
		point: { x: 544, y: 1456 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800131,
		name: '264',
		file: 264,
		map: 109,
		point: { x: 240, y: 1296 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800132,
		name: '265',
		file: 265,
		map: 109,
		point: { x: 336, y: 1256 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800133,
		name: '268',
		file: 268,
		map: 109,
		point: { x: 432, y: 1216 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800134,
		name: '270',
		file: 270,
		map: 109,
		point: { x: 528, y: 1176 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800135,
		name: '272',
		file: 272,
		map: 109,
		point: { x: 624, y: 1136 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800136,
		name: '273',
		file: 273,
		map: 109,
		point: { x: 720, y: 1096 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800137,
		name: '274',
		file: 274,
		map: 109,
		point: { x: 816, y: 1056 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800138,
		name: '275',
		file: 275,
		map: 109,
		point: { x: 912, y: 1016 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800139,
		name: '276',
		file: 276,
		map: 109,
		point: { x: 1008, y: 976 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800140,
		name: '277',
		file: 277,
		map: 109,
		point: { x: 1104, y: 936 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800141,
		name: '278',
		file: 278,
		map: 109,
		point: { x: 1200, y: 896 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800142,
		name: '279',
		file: 279,
		map: 109,
		point: { x: 1300, y: 856 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x88800143,
		name: '280',
		file: 280,
		map: 109,
		point: { x: 1400, y: 816 },
		direction: Direction.SouthWest,
	},
];
