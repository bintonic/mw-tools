import type { NpcJson } from '../../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../../Enums/Direction';

export const ashesPitsNpcsData: NpcJson[] = [
	{
		id: 0x80000334,
		name: 'Dark Shadow',
		file: 159,
		map: 40,
		point: { x: 4000, y: 240 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000335,
		name: 'Sorceror Hades',
		file: 159,
		map: 39,
		point: { x: 656, y: 1496 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000336,
		name: 'Tour Agent',
		file: 159,
		map: 38,
		point: { x: 3760, y: 2880 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000337,
		name: 'Teleporter',
		file: 120,
		map: 37,
		point: { x: 464, y: 512 },
		direction: Direction.SouthWest,
	},
];
